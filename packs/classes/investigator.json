{
  "_id": "s27zdYVFenYBVtfu",
  "name": "Investigator",
  "type": "class",
  "img": "systems/pf1/icons/items/inventory/pipe.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>Whether on the trail of a fugitive, a long-lost treasure trove, or a criminal mastermind, investigators are motivated by an intense curiosity about the world and use knowledge of it as a weapon. Mixing gumption and learnedness into a personal alchemy of daring, investigators are full of surprises. Observing the world around them, they gain valuable knowledge about the situation they’re in, process that information using inspiration and deduction, and cut to the quick of the matter in unexpected ways. Investigators are always evaluating situations they encounter, sizing up potential foes, and looking out for secret dangers, all while using their vast knowledge and powers of perception to find solutions to the most perplexing problems.</p>\n<p><strong>Role</strong>: Investigators live to solve mysteries and find inventive ways to get out of jams. They serve as advisors and support for their adventuring parties, but can take center stage when knowledge and cunning are needed. No slouches in battle, they know how to make surprise attacks and use inspiration to bring those attacks home.</p>\n<p><strong>Alignment</strong>: Any.</p>\n<p><strong>Hit Die</strong>: d8.</p>\n<p><strong>Parent Classes</strong>:&nbsp;<em>Alchemist</em>&nbsp;and&nbsp;<em>rogue</em>.</p>\n<p><strong>Starting Wealth</strong>: 3d6 × 10 gp (average 105 gp.) In addition, each character begins play with an outfit worth 10 gp or less.</p>\n<h2><span id=\"Class_Skills\">Class Skills</span></h2>\n<p>The investigator’s class skills are <em>Acrobatics</em>&nbsp;(<em>Dex</em>), <em>Appraise</em>&nbsp;(<em>Int</em>), <em>Bluff</em>&nbsp;(<em>Cha</em>), <em>Climb</em>&nbsp;(<em>Str</em>), <em>Craft</em>&nbsp;(<em>Int</em>), <em>Diplomacy</em>&nbsp;(<em>Cha</em>), <em>Disable Device</em>&nbsp;(<em>Dex</em>), <em>Disguise</em>&nbsp;(<em>Cha</em>), <em>Escape Artist</em>&nbsp;(<em>Dex</em>), <em>Heal</em>&nbsp;(<em>Wis</em>), <em>Intimidate</em>&nbsp;(<em>Cha</em>), <em>Knowledge</em>&nbsp;(all) (<em>Int</em>), <em>Linguistics</em>&nbsp;(<em>Int</em>), <em>Perception</em>&nbsp;(<em>Wis</em>), <em>Perform</em>&nbsp;(<em>Cha</em>), <em>Profession</em>&nbsp;(<em>Wis</em>), <em>Sense Motive</em>&nbsp;(<em>Wis</em>), <em>Sleight of Hand</em>&nbsp;(<em>Dex</em>), <em>Spellcraft</em>&nbsp;(<em>Int</em>), <em>Stealth</em>&nbsp;(<em>Dex</em>), and <em>Use Magic Device</em>&nbsp;(<em>Cha</em>).</p>\n<p><strong>Skill Ranks per Level</strong>: 6 +&nbsp;<em>Int</em>&nbsp;<em>modifier</em>.</p>"
    },
    "casting": {
      "type": "prepared",
      "progression": "med",
      "ability": "int",
      "spells": "alchemy",
      "cantrips": false
    },
    "tags": [],
    "changes": [],
    "links": {
      "children": [],
      "classAssociations": [
        {
          "id": "pf1.class-abilities.7GNdI4zKfTcxNCk4",
          "dataType": "compendium",
          "name": "Alchemy",
          "img": "systems/pf1/icons/items/potions/magic-yellow.jpg",
          "level": 1,
          "_index": 0
        },
        {
          "id": "pf1.class-abilities.nKbyztRQCU5XMbbs",
          "dataType": "compendium",
          "name": "Inspiration",
          "img": "systems/pf1/icons/skills/emerald_11.jpg",
          "level": 1,
          "_index": 1
        },
        {
          "id": "pf1.class-abilities.C7UgLJt7yRHnAqyG",
          "dataType": "compendium",
          "name": "Poison Lore",
          "img": "systems/pf1/icons/items/potions/minor-green.jpg",
          "level": 2,
          "_index": 2
        },
        {
          "id": "pf1.class-abilities.p3gcIBgwB4NOMrjx",
          "dataType": "compendium",
          "name": "Poison Resistance (INV)",
          "img": "systems/pf1/icons/skills/violet_16.jpg",
          "level": 2,
          "_index": 3
        },
        {
          "id": "pf1.class-abilities.AOnFqb1h3gXbFUup",
          "dataType": "compendium",
          "name": "Investigator Talent",
          "img": "systems/pf1/icons/skills/gray_04.jpg",
          "level": 3,
          "_index": 4
        },
        {
          "id": "pf1.class-abilities.rJBdkI8mlJBNWcU2",
          "dataType": "compendium",
          "name": "Keen Recollection",
          "img": "systems/pf1/icons/skills/red_26.jpg",
          "level": 3,
          "_index": 5
        },
        {
          "id": "pf1.class-abilities.nVwveeNV71Bg7YnY",
          "dataType": "compendium",
          "name": "Studied Combat",
          "img": "systems/pf1/icons/skills/weapon_44.jpg",
          "level": 4,
          "_index": 6
        },
        {
          "id": "pf1.class-abilities.3WsfpUeW7oe4GOuh",
          "dataType": "compendium",
          "name": "Studied Strike",
          "img": "systems/pf1/icons/skills/weapon_29.jpg",
          "level": 4,
          "_index": 7
        },
        {
          "id": "pf1.class-abilities.kEVqxs23oyNDxbc1",
          "dataType": "compendium",
          "name": "Swift Alchemy (INV)",
          "img": "systems/pf1/icons/skills/violet_25.jpg",
          "level": 4,
          "_index": 8
        },
        {
          "id": "pf1.class-abilities.H2Iac6ELVKBU6Ayu",
          "dataType": "compendium",
          "name": "True Inspiration",
          "img": "systems/pf1/icons/skills/shadow_12.jpg",
          "level": 20,
          "_index": 9
        },
        {
          "id": "pf1.class-abilities.NOPkNZh7H143aCMi",
          "dataType": "compendium",
          "name": "Trapfinding (INV)",
          "img": "systems/pf1/icons/items/inventory/lockpick.jpg",
          "level": 1,
          "_index": 10
        },
        {
          "id": "pf1.class-abilities.KbhRBQE5ZyYedJo6",
          "dataType": "compendium",
          "name": "Trap Sense (INV)",
          "img": "systems/pf1/icons/items/inventory/hook-hand.jpg",
          "level": 3,
          "_index": 11
        }
      ]
    },
    "tag": "investigator",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt"]
    },
    "weaponProf": {
      "value": ["sim"],
      "custom": "Hand Crossbow;Rapier;Sap;Shortbow;Short Sword; Sword Cane"
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "bab": "med",
    "skillsPerLevel": 6,
    "savingThrows": {
      "ref": {
        "value": "high"
      },
      "will": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "apr": true,
      "art": true,
      "blf": true,
      "clm": true,
      "crf": true,
      "dip": true,
      "dev": true,
      "dis": true,
      "esc": true,
      "fly": false,
      "han": false,
      "hea": true,
      "int": true,
      "kar": true,
      "kdu": true,
      "ken": true,
      "kge": true,
      "khi": true,
      "klo": true,
      "kna": true,
      "kno": true,
      "kpl": true,
      "kre": true,
      "lin": true,
      "lor": true,
      "per": true,
      "prf": true,
      "pro": true,
      "rid": false,
      "sen": true,
      "slt": true,
      "spl": true,
      "ste": true,
      "sur": false,
      "swm": false,
      "umd": true
    }
  }
}
