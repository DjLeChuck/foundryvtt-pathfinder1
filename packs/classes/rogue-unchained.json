{
  "_id": "5tsZI6zk29yX1ckA",
  "name": "Rogue (Unchained)",
  "type": "class",
  "img": "systems/pf1/icons/items/inventory/lockpick.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>Thief, sneak, charmer, diplomat—all of these and more describe the rogue. When not skulking in the shadows, disarming traps, and stealing from the unaware, rogues may rub shoulders with powerful nobles or plot capers with fellow crooks. The rogue is the master of many faces, using her skills and talents to get herself both into and out of trouble with rakish aplomb. While others may call them charlatans and burglars, even the most larcenous rogues tend to consider themselves consummate professionals, willing to take on any job if the price is right.</p>\n<p><strong>Unchained:</strong> While much of the unchained rogue will be familiar to those who have played the class from the Core Rulebook, there are a number of new class features that greatly enhance the power and flexibility of the rogue. Chief among these is the debilitating injury class feature. A rogue with this ability can severely hamper her foes, giving her a much-needed boost to her offense or defense, depending on the situation. In addition, with finesse training, the rogue now gains Weapon Finesse for free at 1st level. This ability also lets her add her Dexterity to damage rolls with one weapon starting at 3rd level. Finally, the rogue’s edge ability ties into a new system presented in Chapter 2 of this book called skill unlocks. With this feature, the rogue can master a small set of chosen skills, outperforming all those characters without access to such talents.</p>\n<p><strong>Alignment:</strong> Any.</p>\n<p><strong>Hit Die:</strong> d8.</p>\n<p><strong>Starting Wealth:</strong> 4d6 x 10 gp (average 140 gp).</p>\n<h3><strong><span id=\"Class_Skills\">Class Skills</span></strong></h3>\n<p>The rogue (unchained)’s class skills are <em>Acrobatics</em>&nbsp;(<em>Dex</em>), <em>Appraise</em>&nbsp;(<em>Int</em>), <em>Bluff</em>&nbsp;(<em>Cha</em>), <em>Climb</em>&nbsp;(<em>Str</em>), <em>Craft</em>&nbsp;(<em>Int</em>), <em>Diplomacy</em>&nbsp;(<em>Cha</em>), <em>Disable Device</em>&nbsp;(<em>Dex</em>), <em>Disguise</em>&nbsp;(<em>Cha</em>), <em>Escape Artist</em>&nbsp;(<em>Dex</em>), <em>Intimidate</em>&nbsp;(<em>Cha</em>), <em>Knowledge</em>&nbsp;(dungeoneering) (<em>Int</em>), <em>Knowledge</em>&nbsp;(local) (<em>Int</em>), <em>Linguistics</em>&nbsp;(<em>Int</em>), <em>Perception</em>&nbsp;(<em>Wis</em>), <em>Perform</em>&nbsp;(<em>Cha</em>), <em>Profession</em>&nbsp;(<em>Wis</em>), <em>Sense Motive</em>&nbsp;(<em>Wis</em>), <em>Sleight of Hand</em>&nbsp;(<em>Dex</em>), <em>Stealth</em>&nbsp;(<em>Dex</em>), <em>Swim</em>&nbsp;(<em>Str</em>), and <em>Use Magic Device</em>&nbsp;(<em>Cha</em>).</p>\n<p><strong>Skill Ranks per Level</strong>: 8 +&nbsp;<em>Int</em>&nbsp;modifier.</p>"
    },
    "tags": [],
    "changes": [],
    "links": {
      "children": [],
      "classAssociations": [
        {
          "id": "pf1.class-abilities.9l1EOZ2l3wLudvJN",
          "dataType": "compendium",
          "name": "Master Strike (UC)",
          "img": "systems/pf1/icons/feats/greater-vital-strike.jpg",
          "level": 20,
          "_index": 0
        },
        {
          "id": "pf1.class-abilities.HmNDcFTlaEqr60Vw",
          "dataType": "compendium",
          "name": "Finesse Training (UC)",
          "img": "systems/pf1/icons/feats/spellbreaker.jpg",
          "level": 1,
          "_index": 1
        },
        {
          "id": "pf1.class-abilities.6pjr8jMFSKqXkBKk",
          "dataType": "compendium",
          "name": "Sneak Attack (ROG)",
          "img": "systems/pf1/icons/skills/red_13.jpg",
          "level": 1,
          "_index": 2
        },
        {
          "id": "pf1.class-abilities.KQYCRLEdD4bGA5ak",
          "dataType": "compendium",
          "name": "Evasion",
          "img": "systems/pf1/icons/skills/blue_06.jpg",
          "level": 2,
          "_index": 3
        },
        {
          "id": "pf1.class-abilities.iBTrvPtn3jczInbn",
          "dataType": "compendium",
          "name": "Rogue Talents",
          "img": "systems/pf1/icons/feats/acrobatic.jpg",
          "level": 2,
          "_index": 4
        },
        {
          "id": "pf1.class-abilities.nL9Ds9nflmID84vo",
          "dataType": "compendium",
          "name": "Debilitating Injury (UC)",
          "img": "systems/pf1/icons/feats/critical-focus.jpg",
          "level": 4,
          "_index": 5
        },
        {
          "id": "pf1.class-abilities.7WaQxnVaaoL4AGr8",
          "dataType": "compendium",
          "name": "Uncanny Dodge",
          "img": "systems/pf1/icons/feats/wind-stance.jpg",
          "level": 4,
          "_index": 6
        },
        {
          "id": "pf1.class-abilities.ZfnHhhTFQVo0Lj4P",
          "dataType": "compendium",
          "name": "Improved Uncanny Dodge",
          "img": "systems/pf1/icons/feats/wind-stance.jpg",
          "level": 8,
          "_index": 7
        },
        {
          "id": "pf1.class-abilities.iyYyc3rKW0cBZ1cs",
          "dataType": "compendium",
          "name": "Rogue's Edge (UC)",
          "img": "systems/pf1/icons/feats/exotic-weapon-proficiency.jpg",
          "level": 5,
          "_index": 8
        },
        {
          "id": "pf1.class-abilities.EQ7JGo4P1XirLO46",
          "dataType": "compendium",
          "name": "Advanced Talents",
          "img": "systems/pf1/icons/feats/agile-maneuvers.jpg",
          "level": 10,
          "_index": 9
        },
        {
          "id": "pf1.class-abilities.OhHKCLQXoMlYNodk",
          "dataType": "compendium",
          "name": "Trapfinding (UC)",
          "img": "systems/pf1/icons/items/inventory/lockpick.jpg",
          "level": 1,
          "_index": 10
        },
        {
          "id": "pf1.class-abilities.sTlu3zgAEDdJnER5",
          "dataType": "compendium",
          "name": "Danger Sense (ROG)",
          "img": "systems/pf1/icons/feats/tiring-critical.jpg",
          "level": 3,
          "_index": 11
        }
      ]
    },
    "tag": "rogueUnchained",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt"]
    },
    "weaponProf": {
      "value": ["sim"],
      "custom": "Hand Crossbow;Rapier;Sap;Short Sword;Shortbow"
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "bab": "med",
    "skillsPerLevel": 8,
    "savingThrows": {
      "ref": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "apr": true,
      "art": true,
      "blf": true,
      "clm": true,
      "crf": true,
      "dip": true,
      "dev": true,
      "dis": true,
      "esc": true,
      "fly": false,
      "han": false,
      "hea": false,
      "int": true,
      "kar": false,
      "kdu": true,
      "ken": false,
      "kge": false,
      "khi": false,
      "klo": true,
      "kna": false,
      "kno": false,
      "kpl": false,
      "kre": false,
      "lin": true,
      "lor": true,
      "per": true,
      "prf": true,
      "pro": true,
      "rid": false,
      "sen": true,
      "slt": true,
      "spl": false,
      "ste": true,
      "sur": false,
      "swm": true,
      "umd": true
    }
  }
}
